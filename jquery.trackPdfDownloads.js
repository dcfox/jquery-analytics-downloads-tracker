/***
 * jQuery Plugin that adds analytics tracking events to
 * clicks on links that contain a PDF.
 *
 * Code Heavily Borrowed from: https://raw.github.com/jeffreybarke/google-analytics-click-tracking/master/ga-click-tracking-downloads.js
 *
 * Options:  "Category", "Action"
 */

(function($){
    $.fn.trackPDFDownloads = function(options){

        var settings = $.extend({
            category : 'Link Clicks',
            action : 'PDF Download'
        }, options);

        var fileTypes = '\\.(' +
                'pdf|' +
                ')$',
            regExes = {
                download: new RegExp(fileTypes, 'i'),
                external: /^https?\:\/\//i,
                hostName: new RegExp(location.host, 'i')
            },
            isExternal = function (url, rel) {
                // External links have either been marked rel=external
                // or must begin with https?
                // and must not include current host name.
                return ((rel || '').indexOf('external') !== -1) ||
                    (regExes.external.test(url) && !regExes.hostName.test(url));
            },

            getFileName = function (url) {
                var parts = url.split('/');
                return parts[parts.length - 1];
            },

            getFileExt = function (fileName) {
                return fileName.match(regExes.download)[1].toUpperCase();
            },
            setData = function (loc, category, action, label) {
                return {
                    category: settings.category,
                    action: settings.action,
                    label: label,
                    loc: loc
                }

            };

        var $el = jQuery(this),
            url = $el.attr('href'),
            fileName = getFileName(url),
            data = setData(regExes.hostName.test(url) ? url : 'http://' + location.hostname + url, settings.category, settings.action, fileName);

        if (!window._gaq === undefined){
            //Track the Click
            _gaq.push(['_trackEvent', data.category, data.action, data.label]);
        }
        else {
            console.error("The link click was not tracked in analytics because analytics does not appear to be installed on this page.", window._gaq);

        }

        return this;
    }
}(jQuery))